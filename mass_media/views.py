from django.shortcuts import render
from .models import Articles


# Create your views here.

def articles(request):
    articles_list = Articles.objects.all()
    return render(request, "articles_list.html", {'articles_list': articles_list})
