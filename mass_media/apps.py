from django.apps import AppConfig


class MassMediaConfig(AppConfig):
    name = 'mass_media'
