from django.contrib import admin
from .models import Articles


# Register your models here.

class ArticlesAdmin(admin.ModelAdmin):
    list_display = ('date', 'title', 'is_published',)
    list_display_links = ('title',)
    list_filter = ('date',)
    # date_hierarchy = 'date'
    search_fields = ('title', 'short_description',)
    fieldsets = (
        (None, {'fields': ('title', 'short_description', 'description', 'image', 'is_published', 'album',)}),
        ('Настройки', {'fields': ('date', 'author',)}),
    )


admin.site.register(Articles, ArticlesAdmin)
