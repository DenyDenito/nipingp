from django.db import models


# Create your models here.

class TopSlider(models.Model):
    image = models.ImageField(u'Изображение', upload_to='top_slides/', name="image")
    title = models.CharField(u'Заголовок', max_length=500)
    description = models.TextField(u'Описание', max_length=500, blank=True)
    position_number = models.PositiveSmallIntegerField(u'Позиция', default=0)

    class Meta:
        verbose_name = u'Слайд'
        verbose_name_plural = u'Слайды верхнего блока'

    def __str__(self):
        return u'%s' % self.title


class MainBlock(models.Model):
    image = models.ImageField(u'Изображение', upload_to='main_blocks/', name="image")
    url = models.CharField(u'Адрес страницы', max_length=50, blank=True)
    title = models.CharField(u'Заголовок', max_length=150)
    description = models.TextField(u'Описание', max_length=500, blank=True)

    class Meta:
        verbose_name = u'Информационный блок'
        verbose_name_plural = u'Информационные блоки'

    def __str__(self):
        return u'%s' % self.title


class Links(models.Model):
    title = models.CharField(u'Наименование', max_length=50)
    url = models.CharField(u'Адрес страницы', max_length=50)
    block = models.ForeignKey(MainBlock, verbose_name=u'Инфо-блок')

    class Meta:
        verbose_name = u'Ссылка'
        verbose_name_plural = u'Дополнительные ссылки на подразделы'

    def __str__(self):
        return u'%s' % self.title


class TopListBlock(models.Model):
    image = models.ImageField(u'Изображение', upload_to='top_list_blocks/', blank=True)
    title = models.CharField(u'Заголовок', max_length=150, blank=True)
    description = models.TextField(u'Описание', max_length=500)

    class Meta:
        verbose_name = u'Преимущество'
        verbose_name_plural = u'Преимущества компании'

    def __str__(self):
        return u'%s' % self.description[:50]


class PageContent(models.Model):
    title = models.CharField(u'Заголовок', max_length=150)
    description = models.TextField(u'Описание', max_length=1000, blank=True)
    image = models.ImageField(u'Изображение', upload_to='main_page_сontents/', name="image", blank=True)

    class Meta:
        verbose_name = u'Элемент страницы'
        verbose_name_plural = u'Отдельный контент страницы'

    def __str__(self):
        return u'%s' % self.title


class Leadership(models.Model):
    name = models.CharField(u'ФИО', max_length=70)
    position = models.CharField(u'Описание', max_length=150)
    image = models.ImageField(u'Изображение', upload_to='leaders/', name="image")
    biography = models.TextField(u'Описание', max_length=2000, blank=True)
    position_number = models.PositiveSmallIntegerField(u'Позиция', default=0)

    class Meta:
        verbose_name = u'Руководитель'
        verbose_name_plural = u'Руководство компании'

    def __str__(self):
        return u'%s' % self.name


class BottomListBlock(models.Model):
    image = models.ImageField(u'Изображение', upload_to='bottom_list_blocks/', blank=True)
    title = models.CharField(u'Заголовок', max_length=150, blank=True)
    description = models.TextField(u'Описание', max_length=500)

    class Meta:
        verbose_name = u'Принцип'
        verbose_name_plural = u'Наши принципы'

    def __str__(self):
        return u'%s' % self.description[:50]
