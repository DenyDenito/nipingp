from django.contrib import admin
from .models import TopSlider
from .models import MainBlock, Links
from .models import TopListBlock
from .models import PageContent
from .models import Leadership
from .models import BottomListBlock


class TopSlidesAdmin(admin.ModelAdmin):
    class Meta:
        model = TopSlider


class ImageInline(admin.TabularInline):
    model = Links
    extra = 1


class MainBlockAdmin(admin.ModelAdmin):
    class Meta:
        model = MainBlock

    inlines = [ImageInline]

class TopListBlockAdmin(admin.ModelAdmin):
    class Meta:
        model = TopListBlock


class PageContentAdmin(admin.ModelAdmin):
    fields = ('title', 'description')
    exclude = ('image',)

    class Meta:
        model = PageContent

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super(PageContentAdmin, self).get_readonly_fields(request, obj)
        else:
            return 'title'


class LeadershipAdmin(admin.ModelAdmin):
    class Meta:
        model = Leadership


class BottomListBlockAdmin(admin.ModelAdmin):
    class Meta:
        model = BottomListBlock


admin.site.register(TopSlider, TopSlidesAdmin)
admin.site.register(MainBlock, MainBlockAdmin)
admin.site.register(TopListBlock, TopListBlockAdmin)
admin.site.register(PageContent, PageContentAdmin)
admin.site.register(Leadership, LeadershipAdmin)
admin.site.register(BottomListBlock, BottomListBlockAdmin)
