# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2015-11-23 10:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20151120_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='BottomListBlock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, upload_to='bottom_list_blocks/', verbose_name='Изображение')),
                ('title', models.CharField(blank=True, max_length=150, verbose_name='Заголовок')),
                ('description', models.TextField(max_length=500, verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Принцип',
                'verbose_name_plural': 'Наши принципы',
            },
        ),
        migrations.CreateModel(
            name='TopListBlock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, upload_to='top_list_blocks/', verbose_name='Изображение')),
                ('title', models.CharField(blank=True, max_length=150, verbose_name='Заголовок')),
                ('description', models.TextField(max_length=500, verbose_name='Описание')),
            ],
            options={
                'verbose_name': 'Преимущество',
                'verbose_name_plural': 'Преимущества компании',
            },
        ),
        migrations.DeleteModel(
            name='Feature',
        ),
    ]
