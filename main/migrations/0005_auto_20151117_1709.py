# -*- coding: utf-8 -*-
# Generated by Django 1.9b1 on 2015-11-17 12:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20151117_1534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagecontent',
            name='image',
            field=models.ImageField(blank=True, upload_to='main_page_сontent_images/', verbose_name='Изображение'),
        ),
    ]
