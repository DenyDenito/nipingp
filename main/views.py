from django.shortcuts import render
from .models import TopSlider
from .models import MainBlock
from .models import TopListBlock
from .models import PageContent
from .models import Leadership
from news.models import News
from .models import BottomListBlock
from gallery.models import Album


# Create your views here.

def main(request):
    top_slides = TopSlider.objects.all().order_by("position_number")
    feature_list = TopListBlock.objects.all()
    info_blocks = MainBlock.objects.all()
    contents_list = PageContent.objects.all()
    leaders_list = Leadership.objects.all().order_by("position_number")
    news_list = News.objects.all().order_by('-id')[:3]
    principles_list = BottomListBlock.objects.all()
    image_list = Album.objects.first().image_set.all().filter(from_index=True)
    return render(request, "index-cover.html",
                  {'top_slides': top_slides, 'feature_list': feature_list, 'info_blocks': info_blocks,
                   'news_list': news_list, 'contents_list': contents_list, 'leaders_list': leaders_list,
                   'principles_list': principles_list, 'image_list': image_list})


def test(request):
    return render(request, "index_test.html")
