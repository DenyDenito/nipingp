from django.db import models

# Create your models here.


class Album(models.Model):
    title = models.CharField(u'Наименование', max_length=255)
    description = models.CharField(u'Описание', max_length=1000, blank=True)

    class Meta:
        verbose_name = u'Альбом'
        verbose_name_plural = u'Альбомы'

    def __str__(self):
        return u'%s' % self.title

    def get_main_image(self):
        return self.image_set.all().filter(is_main=True).first()

    def get_visible_images(self):
        return self.image_set.all().exclude(is_hide=True)


class Image(models.Model):
    picture = models.ImageField(u'Изображение', upload_to='gallery/')
    title = models.CharField(u'Наименование', max_length=255, blank=True)
    description = models.CharField(u'Описание', max_length=1000, blank=True)
    is_main = models.BooleanField(u'Основное изображение альбома', default=False)
    from_index = models.BooleanField(u'Для главной страницы', default=False)
    is_hide = models.BooleanField(u'Скрыть в альбоме', default=False)
    album = models.ForeignKey(Album, verbose_name=u'Альбом')

    class Meta:
        verbose_name = u'Фотография'
        verbose_name_plural = u'Фотографии'

    def __str__(self):
        return u'%s' % self.title
