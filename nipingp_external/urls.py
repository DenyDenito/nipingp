from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^history/$', views.history, name='history'),
    url(r'^services/$', views.services, name='services'),
    url(r'^projects/$', views.projects, name='projects'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^partnership/$', views.partnership, name='partnership'),
    url(r'^safely/$', views.safely, name='safely'),
    url(r'^social/$', views.social, name='social'),
    url(r'^career/$', views.career, name='career'),
    url(r'^career/vacancies/$', views.vacancies, name='vacancies'),
    url(r'^ecology/$', views.ecology, name='ecology'),
    url(r'^ecology/competencies/$', views.competencies, name='competencies'),
    url(r'^worth/$', views.worth, name='worth'),
]
