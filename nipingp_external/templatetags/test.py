from main.models import PageContent
from django import template

register = template.Library()


@register.simple_tag
def nav_content():
    contents_list = PageContent.objects.all()
    return {
        'brand': contents_list[0].description,
        'tel_number': contents_list[1].description,
    }
