from django.shortcuts import render
from .models import Vacancies

# Create your views here.


def history(request):
    return render(request, "history.html")


def services(request):
    return render(request, "services.html")


def projects(request):
    return render(request, "projects.html")


def contacts(request):
    return render(request, "contacts.html")


def partnership(request):
    return render(request, "partnership.html")


def safely(request):
    return render(request, "us_safely.html")


def social(request):
    return render(request, "social_responsibility.html")


def career(request):
    return render(request, "career.html")


def vacancies(request):
    vacancies_list = Vacancies.objects.all()
    return render(request, "vacancies.html", {'vacancies_list': vacancies_list})


def ecology(request):
    return render(request, "ecology.html")


def competencies(request):
    return render(request, "core_competencies.html")


def worth(request):
    return render(request, "worth.html")
