from django.apps import AppConfig


class NipingpExternalConfig(AppConfig):
    name = 'nipingp_external'
