from django.db import models

# Create your models here.


class Vacancies(models.Model):
    title = models.CharField(u'Заголовок', max_length=120)
    description = models.TextField(u'Описание', max_length=350)

    class Meta:
        verbose_name = u'Вакансии'
        verbose_name_plural = u'Вакансии компании'

    def __str__(self):
        return u'%s' % self.title
