from django.contrib import admin
from .models import News


# Register your models here.

class NewsAdmin(admin.ModelAdmin):
    class Meta:
        model = News


admin.site.register(News, NewsAdmin)
