from django.db import models


# Create your models here.

class News(models.Model):
    image = models.ImageField(u'Изображение', upload_to='news/')
    title = models.CharField(u'Заголовок', max_length=120)
    short_description = models.TextField(u'Краткое описание', max_length=300)
    description = models.TextField(u'Описание', max_length=2000)

    class Meta:
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости компании'

    def __str__(self):
        return u'%s' % self.title
