from django.db import models
from gallery.models import Album


# Create your models here.

class Publications(models.Model):
    image = models.ImageField(u'Изображение', upload_to='publications/')
    title = models.CharField(u'Заголовок', max_length=120)
    author = models.CharField(u'Автор', max_length=120)
    date = models.CharField(u'Дата публикации', max_length=120)
    is_published = models.BooleanField(u'Опубликовать в разделе?', default=True)
    short_description = models.TextField(u'Описание', max_length=150, blank=True)
    description = models.TextField(u'Описание', max_length=3000, blank=True)
    album = models.ForeignKey(Album, verbose_name=u'Альбом', blank=True)

    class Meta:
        verbose_name = u'Публикация'
        verbose_name_plural = u'Научные публикации'

    def __str__(self):
        return u'%s' % self.title

    @models.permalink
    def get_absolute_url(self):
        return 'publication_detail', (), {"pk": self.pk}
